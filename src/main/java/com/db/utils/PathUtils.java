package com.db.utils;

import java.nio.file.Paths;

public class PathUtils {

    // 项目路径
    public static final String dirUser = System.getProperty("user.dir");

    // 缓存路径
    public static final String path_cache = Paths.get(dirUser, "cache").toString();

    // 日志文件
    public static final String file_log_threadPool = "threadpool.txt";
//    public static final String file_log_app = "app_" + DateUtil.getHH() + ".txt";

    public static String getCacheDir() {
        return path_cache;
    }

    public static String getAppLogFile() {
        return Paths.get(path_cache, DateUtil.getTodayFolder(), "app_" + DateUtil.getHH() + ".txt").toString();
    }

    public static String getThreadPoolLogFile() {
        return Paths.get(path_cache, DateUtil.getTodayFolder(), file_log_threadPool).toString();
    }
}
