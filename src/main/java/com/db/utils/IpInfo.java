package com.db.utils;

public class IpInfo {
    /**
     * code : 0
     * msg :
     * data : {"ip":"106.66.158.8","proxyIp":"172.105.56.133","proxyPort":22964,"username":"","password":"","iso":"IN","mcc":"404","mnc":"019","networkOperatorName":"Idea","provider":"leo-proxy","androidId":"","city":"","channelId":-1,"proxyType":1,"networkType":"-1","networkSubtype":"-1","networkOperator":"40419"}
     */

    private int code;
    private String msg;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ip : 106.66.158.8
         * proxyIp : 172.105.56.133
         * proxyPort : 22964
         * username :
         * password :
         * iso : IN
         * mcc : 404
         * mnc : 019
         * networkOperatorName : Idea
         * provider : leo-proxy
         * androidId :
         * city :
         * channelId : -1
         * proxyType : 1
         * networkType : -1
         * networkSubtype : -1
         * networkOperator : 40419
         */

        private String ip = "";
        private String proxyIp;
        private int proxyPort;
        private String username;
        private String password;
        private String iso;
        private String mcc;
        private String mnc;
        private String networkOperatorName;
        private String provider;
        private String androidId;
        private String city;
        private int channelId;
        private int proxyType;
        private String networkType;
        private String networkSubtype;
        private String networkOperator;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getProxyIp() {
            return proxyIp;
        }

        public void setProxyIp(String proxyIp) {
            this.proxyIp = proxyIp;
        }

        public int getProxyPort() {
            return proxyPort;
        }

        public void setProxyPort(int proxyPort) {
            this.proxyPort = proxyPort;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getIso() {
            return iso;
        }

        public void setIso(String iso) {
            this.iso = iso;
        }

        public String getMcc() {
            return mcc;
        }

        public void setMcc(String mcc) {
            this.mcc = mcc;
        }

        public String getMnc() {
            return mnc;
        }

        public void setMnc(String mnc) {
            this.mnc = mnc;
        }

        public String getNetworkOperatorName() {
            return networkOperatorName;
        }

        public void setNetworkOperatorName(String networkOperatorName) {
            this.networkOperatorName = networkOperatorName;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public String getAndroidId() {
            return androidId;
        }

        public void setAndroidId(String androidId) {
            this.androidId = androidId;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public int getProxyType() {
            return proxyType;
        }

        public void setProxyType(int proxyType) {
            this.proxyType = proxyType;
        }

        public String getNetworkType() {
            return networkType;
        }

        public void setNetworkType(String networkType) {
            this.networkType = networkType;
        }

        public String getNetworkSubtype() {
            return networkSubtype;
        }

        public void setNetworkSubtype(String networkSubtype) {
            this.networkSubtype = networkSubtype;
        }

        public String getNetworkOperator() {
            return networkOperator;
        }

        public void setNetworkOperator(String networkOperator) {
            this.networkOperator = networkOperator;
        }

        @Override
        public String toString() {
            return "ProxyIp:" + getProxyIp()
                    + ", ProxyProt:" + getProxyPort()
                    + ", Username:" + getUsername()
                    + ", Password:" + getPassword();
        }
    }
}
