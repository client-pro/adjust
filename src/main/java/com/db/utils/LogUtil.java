package com.db.utils;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtil {

    private final static boolean DEBUG = true;

    private final static boolean LOGSAVE = true;

    //    private final static String TAG = "AppsFlyer";
    private final static String TAG = "Adjust";

    public static void log(String msg) {
        log(null, msg);
    }

    public static void log(String packageName, String deviceId, String msg) {
        if (DEBUG) {
            StringBuffer content = new StringBuffer();
            content.append(getCurrentTime()).append(" ").append(TAG);

            content.append("_").append(deviceId);
            content.append("_" + ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);
            content.append(":");
            content.append(msg);
            System.out.println(content);
            if (LOGSAVE) {
                if (deviceId != null) {
//                    FileUtils.appendFileLine(PathUtils.getDeviceDir(packageName, deviceId)+"log",content.toString());
                    return;
                }
                if (packageName != null) {
//                    FileUtils.appendFileLine(PathUtils.getPackageTaskDir(packageName)+"log",content.toString());
                    return;
                }
//                FileUtils.appendFileLine(PathUtils.getCacheDir()+"log",content.toString());
//                FileUtils.appendFileLine(PathUtils.getDeviceDir(InstallEngin.packageTask, InstallEngin.deviceId)+"log",content.toString());
            }
        }
    }


    public static void log(String tag, String msg) {
        if (DEBUG) {
//            String packageTask = InstallEngin.packageTask == null? ClickEngin.packageTask : InstallEngin.packageTask;
//            String deviceId = InstallEngin.deviceId == null? ClickEngin.deviceId : InstallEngin.deviceId;
            StringBuffer content = new StringBuffer();
            content.append(getCurrentTime()).append("|").append(TAG);
            if (tag != null && !"".equals(tag)) {
                content.append("_").append(tag);
            }
//            content.append("_").append(deviceId);
            content.append("_" + ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);
            content.append(":");
            content.append(msg);
            System.out.println(content);

//            if(LOGSAVE){
//                if(deviceId != null){
//                    FileUtils.appendFileLine(PathUtils.getDeviceDir(packageTask, deviceId)+"log",content.toString());
//                    return;
//                }
//                if(packageTask != null){
//                    FileUtils.appendFileLine(PathUtils.getPackageTaskDir(packageTask)+"log",content.toString());
//                    return;
//                }
//                FileUtils.appendFileLine(PathUtils.getCacheDir()+"log",content.toString());
////                FileUtils.appendFileLine(PathUtils.getDeviceDir(InstallEngin.packageTask, InstallEngin.deviceId)+"log",content.toString());
//            }
        }
    }

    public static void logApp(String tag, String msg) {
        // 日期|标签|[日志内容]
        if (DEBUG) {
            StringBuffer content = new StringBuffer();
            content.append(getCurrentTime()).append("|").append(TAG);
            content.append("_").append(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);

            if (tag != null && !tag.isEmpty()) {
                content.append("|").append(tag);
            }

            content.append("|").append(msg);
            System.out.println(content);

            if (LOGSAVE) {
                FileUtils.appendFileLine(PathUtils.getAppLogFile(), content.toString());
            }
        }
    }

    public static void logThreadPool(String tag, String msg) {
        if (DEBUG) {
            StringBuffer content = new StringBuffer();
            content.append(getCurrentTime()).append("|").append(TAG);
            content.append("_").append(ManagementFactory.getRuntimeMXBean().getName().split("@")[0]);

            if (tag != null && !tag.isEmpty()) {
                content.append("|").append(tag);
            }

            content.append("|").append(msg);
            System.out.println(content);

            if (LOGSAVE) {
                FileUtils.appendFileLine(PathUtils.getThreadPoolLogFile(), content.toString());
            }
        }
    }

    public static String getCurrentTime() {
//        long currentTime = System.currentTimeMillis();
//        Date date = new Date(currentTime + 24 * 60 * 60 * 1000);
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return format.format(date);
    }
}
