package com.db.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

/**
 * 配置文件工具
 */
public class PropertiesUtil {
    private final static Properties properties = new Properties();

    private static final String ROOT_DIR = "root.dir";

    // Unidbg 签名接口
    private static final String SIGNER_API = "signer.api";
    private static final String SIGNER_API_ADJ5 = "signer.api.adj5";
    private static final String DEFAULT_UNIDBG_SIGNER = "http://localhost:8899/signer";

    static {
        try {
            File configFile = FileUtils.getConfigFile();
            properties.load(Files.newInputStream(configFile.toPath()));

            if (!properties.containsKey(SIGNER_API)) {
                properties.setProperty(SIGNER_API, DEFAULT_UNIDBG_SIGNER);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getSignerApi(String algoType) {

        try {
            if ("adj5".equalsIgnoreCase(algoType)) {
                return properties.getProperty(SIGNER_API_ADJ5, DEFAULT_UNIDBG_SIGNER);
            } else {
                return properties.getProperty(SIGNER_API, DEFAULT_UNIDBG_SIGNER);
            }
        } catch (Exception e) {
            LogUtil.logApp("getProperty", "获取[SIGNER_API]配置发生异常");
            return "";
        }
    }
}
