package com.db.utils;

import java.util.HashMap;
import java.util.Map;

public class ISOMap {

    /**
     * iso时区代码
     * {@link <a href="https://timezonedb.com/time-zones">iso时区代码</a>}
     */
    public static final Map<String, String> ISO_TIME_ZONE_MAP = new HashMap<String, String>() {{
        put("SA", "Asia/Riyadh");
        put("VN", "Asia/Ho_Chi_Minh");
        put("RU", "Europe/Moscow");
        put("BR", "America/Sao_Paulo");
        put("AE", "Asia/Dubai");
        put("IN", "Asia/Kolkata");
        put("ID", "Asia/Jakarta");
        put("MX", "America/Mexico_City");
        put("TR", "Europe/Istanbul");
        put("DE", "Europe/Berlin");
        put("KR", "Asia/Seoul");
        put("GB", "Europe/London");
        put("FR", "Europe/Paris");
        put("TH", "Asia/Bangkok");
        put("JO", "Asia/Amman");
        put("EG", "Africa/Cairo");
        put("GT", "America/Guatemala");
        put("ES", "Europe/Madrid");
        put("PK", "Asia/Karachi");
    }};


    /**
     * iso语言代码
     * {@link <a href="https://zh.wikipedia.org/wiki/ISO_639-1">ISO 639-1</a>}
     */
    public static final Map<String, String> ISO_LANGUAGE_MAP = new HashMap<String, String>() {{
        put("SA", "ar");
        put("VN", "vi");
        put("RU", "ru");
        put("BR", "pt");
        put("AE", "ar");
        put("IN", "en");
        put("ID", "in");
        put("MX", "es");
        put("TR", "tr");
        put("DE", "de");
        put("KR", "ko");
        put("GB", "en");
        put("US", "en");
        put("FR", "fr");
        put("CA", "en");
        put("PH", "en");
        put("TH", "th");
        put("JO", "jo");
        put("EG", "eg");
        put("GT", "gt");
        put("ES", "es");
        put("PK", "pk");
    }};


    /**
     * MCC(移动国家码)和 MNC(移动网络码)
     * {@link <a href="https://blog.csdn.net/bytxl/article/details/48345581">MCC(移动国家码)和 MNC(移动网络码)</a>}
     */
    public static final Map<String, String> ISO_MCC_MAP = new HashMap<String, String>() {{
        put("SA", "420");
        put("VN", "452");
        put("RU", "250");
        put("BR", "724");
        put("AE", "424");
        put("IN", "404");
        put("ID", "510");
        put("MX", "334");
        put("TR", "286");
        put("DE", "262");
        put("KR", "450");
        put("GB", "234");
        put("US", "310");
        put("FR", "208");
        put("CA", "302");
        put("PH", "515");
        put("TH", "520");
        put("JO", "416");
        put("EG", "602");
        put("GT", "704");
        put("ES", "214");
        put("PK", "410");
    }};
}
