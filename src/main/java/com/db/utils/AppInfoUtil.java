package com.db.utils;

import cn.hutool.log.Log;
import com.db.main.IOSTask;
import com.db.main.Task;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.db.main.Constant.BASE_URL;
import static com.db.main.Constant.GET_APPINFO;

public class AppInfoUtil {

    public static final Map<String, AppInfo> appInfoCached = new HashMap<>();

    public static AppInfo getAppInfo(String packageName, String homePlt) {
        AppInfo appInfo = appInfoCached.get(packageName);
        if (appInfo == null) {
            appInfo = getAppInfoFromServer(packageName, homePlt);
            if (appInfo != null) {
                synchronized (appInfoCached) {
                    appInfoCached.put(packageName, appInfo);
                }
            }
            return appInfo;
        }
        checkAppInfo(appInfo);
        return appInfo;
    }

    private static AppInfo getAppInfoFromServer(String packageName, String homePlt) {
        // 获取AppInfo信息
//        String ret = Downloader.doDownload(String.format(BASE_URL + GET_APPINFO, packageName, homePlt), "GET", null, null);
        String ret = Downloader.doDownload(String.format(BASE_URL + GET_APPINFO, packageName, homePlt), "GET", null, null);
        JSONObject jsonObject = new JSONObject(ret);
        Object data = jsonObject.get("data");
        LogUtil.logApp("AppInfo", "packageName:" + packageName + ", getAppInfoFromServer:" + ret);
        if (jsonObject.getInt("code") == 200 && data != JSONObject.NULL) {
            AppInfo appInfo = new AppInfo();
            JSONObject dataJson = (JSONObject) data;
            appInfo.appInfo = dataJson.getString("info");
            appInfo.homePlt = homePlt;
            appInfo.target = dataJson.getString("target");
            appInfo.refreshTime = System.currentTimeMillis();

            appInfo.algorithm = dataJson.optString("algorithm", "");
            appInfo.signatureMeta = dataJson.optString("signatureMeta", "");
            appInfo.programme = dataJson.optString("programme", "");
            // adj算法类型，如：adj5
            appInfo.way = dataJson.optString("way", "");

            appInfo.campaign = dataJson.optString("campaign", "");
            appInfo.adgroup = dataJson.optString("adgroup", "");
            appInfo.creative = dataJson.optString("creative", "");
            appInfo.callback = dataJson.optString("callback", "");
            return appInfo;
        }

        return null;
    }

    private static void checkAppInfo(AppInfo appInfo) {
        if (System.currentTimeMillis() - appInfo.refreshTime > TimeUnit.MINUTES.toMillis(3)) {//3 minutes before
            new Thread() {
                @Override
                public void run() {
                    AppInfo newAppInfo = getAppInfoFromServer(appInfo.packageName, appInfo.homePlt);
                    if (newAppInfo == null) {
                        return;
                    }
                    synchronized (appInfoCached) {
                        appInfoCached.put(newAppInfo.packageName, newAppInfo);
                    }
                }
            }.start();
        }
    }

    public static class AppInfo {
        public String packageName;
        public String target;
        public String appInfo;
        public String homePlt;
        public long refreshTime;

        // 纯算数据
        public String algorithm;
        // App签名数据
        public String signatureMeta;
        // 签名处理类型，0:unidbg, 1:纯算, 2:rpc
        public String programme;
        // adj算法版本，如：adj5
        public String way;

        public String campaign;
        public String adgroup;
        public String creative;
        public String callback;
    }
}
