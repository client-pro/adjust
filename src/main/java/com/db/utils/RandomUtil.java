package com.db.utils;

import java.util.Random;

public class RandomUtil {
    private static Random random;

    public static Random getInstance() {
        if (random == null) {
            random = new Random();
        }
        return random;
    }

    public static boolean getPercentTrue(int percent) {
        return getInstance().nextInt(100) < percent;
    }

    public static int nextInt(int limit) {
        return getInstance().nextInt(limit);
    }

    public static int getRange(int base, int range) {
        return base + nextInt(range);
    }

    public static long nextLong(long range) {
        return getInstance().nextLong();
    }

    //include base;exclude base+rang;
    public static long getRange(long base, int range) {
        return base + nextInt(range);
    }

}
