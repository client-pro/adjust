package com.db.main;

import com.db.utils.RandomUtil;
import com.db.utils.Util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class IOSDevice {

    /**
     * iOS系统大版本
     */
    private static final String[] MAJOR_OS_VERSION = {"14", "15", "16", "17"};

    /**
     * 系统大版本及对应的设备代号
     */
    private static final Map<String, String[]> DEVICES = new HashMap<>();

    /**
     * 系统大版本及对应的系统版本号
     */
    private static final Map<String, String[]> OS_VERSIONS = new HashMap<>();

    public String deviceName;
    public final static String deviceType = "iPhone";
    public final static String osName = "ios";
    public String osVersion;
    public String idfa;
    public String idfv;
    public String ua;
    public String startTime;

    public static final String uaTemplate = "Mozilla/5.0 (iPhone; CPU iPhone OS %s like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/%s Mobile/15E148 Safari/604.1";

    static {
        // 系统大版本 -> 设备代号
        DEVICES.put("14", new String[]{
                "iPhone8,4", "iPhone12,8",
                "iPhone8,1", "iPhone8,2",
                "iPhone9,1", "iPhone9,2", "iPhone9,3", "iPhone9,4",
                "iPhone10,1", "iPhone10,2", "iPhone10,4", "iPhone10,5",
                "iPhone10,3", "iPhone10,6", "iPhone11,2", "iPhone11,4", "iPhone11,6", "iPhone11,8",
                "iPhone12,1", "iPhone12,3", "iPhone12,5", "iPhone12,8",
                "iPhone13,1", "iPhone13,2", "iPhone13,3", "iPhone13,4"
        });
        DEVICES.put("15", new String[]{
                "iPhone8,1", "iPhone8,2",
                "iPhone9,1", "iPhone9,2", "iPhone9,3", "iPhone9,4",
                "iPhone10,1", "iPhone10,2", "iPhone10,4", "iPhone10,5",
                "iPhone10,3", "iPhone10,6", "iPhone11,2", "iPhone11,4", "iPhone11,6", "iPhone11,8",
                "iPhone12,1", "iPhone12,3", "iPhone12,5", "iPhone12,8",
                "iPhone13,1", "iPhone13,2", "iPhone13,3", "iPhone13,4",
                "iPhone14,2", "iPhone14,3", "iPhone14,4", "iPhone14,5"
        });
        DEVICES.put("16", new String[]{
                "iPhone12,8", "iPhone14,6",
                "iPhone10,1", "iPhone10,2", "iPhone10,4", "iPhone10,5",
                "iPhone10,3", "iPhone10,6", "iPhone11,2", "iPhone11,4", "iPhone11,6", "iPhone11,8",
                "iPhone12,1", "iPhone12,3", "iPhone12,5", "iPhone12,8",
                "iPhone13,1", "iPhone13,2", "iPhone13,3", "iPhone13,4",
                "iPhone14,2", "iPhone14,3", "iPhone14,4", "iPhone14,5",
                "iPhone14,7", "iPhone14,8", "iPhone15,2", "iPhone15,3",
        });
        DEVICES.put("17", new String[]{
                "iPhone12,8", "iPhone14,6",
                "iPhone11,2", "iPhone11,4", "iPhone11,6", "iPhone11,8",
                "iPhone12,1", "iPhone12,3", "iPhone12,5", "iPhone12,8",
                "iPhone13,1", "iPhone13,2", "iPhone13,3", "iPhone13,4",
                "iPhone14,2", "iPhone14,3", "iPhone14,4", "iPhone14,5",
                "iPhone14,7", "iPhone14,8", "iPhone15,2", "iPhone15,3",
                "iPhone15,4", "iPhone15,5", "iPhone16,1", "iPhone16,2",
        });

        // 系统大版本 -> 小版本
        OS_VERSIONS.put("14", new String[]{
                "14.0", "14.0.1",
                "14.1",
                "14.2", "14.2.1",
                "14.3",
                "14.4", "14.4.1", "14.4.2",
                "14.5", "14.5.1",
                "14.6",
                "14.7",
                "14.8", "14.8.1"
        });

        OS_VERSIONS.put("15", new String[]{
                "15.0", "15.0.1", "15.0.2",
                "15.1", "15.1.1",
                "15.2", "15.2.1",
                "15.3", "15.3.1",
                "15.4", "15.4.1",
                "15.5",
                "15.6", "15.6.1",
                "15.7", "15.7.1", "15.7.2", "15.7.3", "15.7.4", "15.7.5", "15.7.6", "15.7.7", "15.7.8", "15.7.9",
                "15.8", "15.8.1", "15.8.2"
        });

        OS_VERSIONS.put("16", new String[]{
                "16.0", "16.0.1", "16.0.2", "16.0.3",
                "16.1", "16.1.1", "16.1.2",
                "16.2",
                "16.3", "16.3.1",
                "16.4", "16.4.1",
                "16.5", "16.5.1",
                "16.6", "16.6.1",
                "16.7", "16.7.1", "16.7.2", "16.7.3", "16.7.4", "16.7.5", "16.7.6", "16.7.7", "16.7.8"
        });

        OS_VERSIONS.put("17", new String[]{
                "17.0", "17.0.1", "17.0.2", "17.0.3",
                "17.1", "17.1.1", "17.1.2",
                "17.2",
                "17.3", "17.3.1",
                "17.4", "17.4.1",
                "17.5", "17.5.1"
        });
    }

    public static IOSDevice getInstance(String iso) {
        IOSDevice instance = new IOSDevice();

        // 随机设备信息
        String major = getRandomMajor();
        instance.deviceName = getRandomDeviceName(major);
        instance.osVersion = getRandomOsVersion(major);
        instance.idfa = UUID.randomUUID().toString().toUpperCase();
        instance.idfv = UUID.randomUUID().toString().toUpperCase();

        instance.ua = String.format(uaTemplate, instance.osVersion.replace('.', '_'), instance.osVersion);
        instance.startTime = Util.format(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(RandomUtil.getRange(24, 240)), iso);

        return instance;
    }

    public static String getRandomMajor() {
        return MAJOR_OS_VERSION[RandomUtil.getRange(0, MAJOR_OS_VERSION.length)];
    }

    public static String getRandomDeviceName(String major) {
        return DEVICES.get(major)[RandomUtil.getRange(0, DEVICES.get(major).length)];
    }

    public static String getRandomOsVersion(String major) {
        return OS_VERSIONS.get(major)[RandomUtil.getRange(0, OS_VERSIONS.get(major).length)];
    }

    @Override
    public String toString() {
        return String.format("deviceName:%s, osVersion:%s, idfa:%s, idfv:%s, ua:%s", deviceName, osVersion, idfa, idfv, ua);
    }
}
