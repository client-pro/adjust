package com.db.main;

import com.db.utils.RandomUtil;
import com.db.utils.Util;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class IOSDeviceBak {
//    device_name  iPhone10,3
//    device_type  iPhone
//    os_name	     ios
//    os_version   16.3
//    idfa	     7D58C6D1-33AC-4B72-A180-DAB946EDBD93
//    idfv       B3DEC5D0-A6DF-4247-88DD-75A9C29824A6

    public String deviceName;
    public final static String deviceType = "iPhone";
    public final static String osName = "ios";
    public String osVersion;
    public String idfa;
    public String idfv;

    public String startTime;

    //https://en.wikipedia.org/wiki/List_of_iPhone_models
    public static String[] deviceNames = {
            "iPhone9,1", "iPhone9,2", "iPhone9,3", "iPhone9,4",
            "iPhone10,3", "iPhone10,6", "iPhone10,2", "iPhone10,5", "iPhone10,1", "iPhone10,4",
            "iPhone11,2", "iPhone11,4", "iPhone11,6", "iPhone11,8",
            "iPhone12,1", "iPhone12,3", "iPhone12,5", "iPhone12,8",
            "iPhone13,1", "iPhone13,2", "iPhone13,3", "iPhone13,4",
            "iPhone14,2", "iPhone14,3", "iPhone14,4", "iPhone14,5", "iPhone14,6", "iPhone14,7", "iPhone14,8",
            "iPhone15,2", "iPhone15,3", "iPhone15,4", "iPhone15,5",
            "iPhone16,1", "iPhone16,2"
    };

    public static String[] osVersions = {
            "15", "15.0.1", "15.0.2", "15.1", "15.1.1",
            "15.2", "15.2.1", "15.3", "15.3.1", "15.4",
            "15.4.1", "15.5", "15.6", "15.6.1", "15.7", "15.7.1",
            "16", "16.0.2", "16.0.3", "16.1", "16.1.1",
            "16.1.2", "16.2", "16.3", "16.3.1", "16.4",
            "16.4.1", "16.5", "16.5.1", "16.6", "16.6.1",
            "16.7", "16.7.1", "16.7.2",
            "17.0.1", "17.0.2", "17.0.3", "17.1", "17.1.1",
            "17.1.2", "17.2", "17.2.1", "17.3", "17.3.1"
    };

    //"Mozilla/5.0 (iPhone; CPU iPhone OS 16_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.3 Mobile/15E148 Safari/604.1";

    public static final String uaTemplate = "Mozilla/5.0 (iPhone; CPU iPhone OS %s like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/%s Mobile/15E148 Safari/604.1";
    public String ua;

    public static IOSDeviceBak getInstance(String iso) {
        IOSDeviceBak instance = new IOSDeviceBak();
//        instance.deviceName = "iPhone8,1";
//        instance.osVersion = "14.2";

        // 不太准确，可能会被检测虚假设备
        instance.deviceName = deviceNames[RandomUtil.getRange(0, deviceNames.length)];
        instance.osVersion = osVersions[RandomUtil.getRange(0, osVersions.length)];

        instance.idfa = UUID.randomUUID().toString().toUpperCase();
        instance.idfv = UUID.randomUUID().toString().toUpperCase();
        String ua = String.format(uaTemplate, instance.osVersion.replace('.', '_'), instance.osVersion);
        instance.ua = ua;
//        System.currentTimeMillis() - TimeUnit.DAYS.toSeconds(1);
        instance.startTime = Util.format(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(RandomUtil.getRange(24, 240)), iso);
        return instance;
    }

    @Override
    public String toString() {
//        return String.format("deviceName:%s\nosVersion:%s\nidfa:%s\nidfv:%s\nua:%s",deviceName,osVersion,idfa,idfv,ua);
        return String.format("deviceName:%s, osVersion:%s, idfa:%s, idfv:%s, ua:%s", deviceName, osVersion, idfa, idfv, ua);
    }

    public static void main(String[] args) {
//        System.out.println(getInstance(null));
        System.out.println(osVersions[RandomUtil.getRange(0, osVersions.length)]);
        System.out.println(deviceNames[RandomUtil.getRange(0, deviceNames.length)]);
    }
}
