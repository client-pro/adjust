package com.db.main;

public class Constant {
    public static final String BASE_URL = "https://pb.doubletick.site";

    public static final String IP_URL = "http://wa-business.union169.com/api/ip/getIp";

    public static final String GET_OFFER_PATH = "/admin/task/getOfferTask";

    public static final String GET_APPINFO = "/api/pkgInfo/getPkgInfoByPakAndHomePlatform?pkg=%s&homePlatform=%s";

    public static final String VERIFY_TOKEN = "a88d037d0a10be1ba5b8ffa88b1c29f5";

    public static final String ADJUST_URL = "https://app.adjust.com";

    // 2024/6/7 ip字段经常为null
    public static final String VERIFY_PROXY_LUMTEST_URL = "https://lumtest.com/myip.json";

    // 2024/6/7 地区有时候为空
    public static final String VERIFY_PROXY_USERAGENTINFO_URL = "https://ip.useragentinfo.com/json";

    public static final String VERIFY_PROXY_IPINFO_URL = "https://ipinfo.io/json";

    // 黑名单IP过滤接口：Yandex单子
    public static final String VERIFY_IP_BLACKLIST_YANDEX = "https://pb.doubletick.site/ip/api/checkIp?ip=";

    // 黑名单IP过滤接口
    public static final String VERIFY_IP_BLACKLIST = "https://pb.doubletick.site/admin/task/checkIpBlack?ip=";

    public static final String VIRTUAL_ANDROID_DEVICE_URL = "http://47.57.14.187:18089/api/task/android/random?taskId=%s&random=%s";

    // 临时使用
    public static final String VIRTUAL_ANDROID_DEVICE_URL2 = "http://8.210.55.226:18089/api/task/android/random?taskId=%s&random=%s";

    public static final String VERIFY_GAID_URL = "https://pb.doubletick.site/detectionGaidBlack?gaid=%s&gaidBlackWhiteOrderId=%s";

    public static final String REAL_ANDROID_DEVICE_URL = "http://8.210.55.226:18089/api/task/android/randomEs?taskId=%s";

    // 上传留存信息
    public static final String ALIVE_URL = "https://pb.doubletick.site/admin/keepData/uploadKeepData";

}
