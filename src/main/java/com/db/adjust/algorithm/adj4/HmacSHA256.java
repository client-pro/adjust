package com.db.adjust.algorithm.adj4;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


public class HmacSHA256 {
    public static String sha256_HMAC(String HexMessage, String HexSecretKey) {
        String hash = "";
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(AESUtil.hexToByteArray(HexSecretKey), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] bytes = sha256_HMAC.doFinal(AESUtil.hexToByteArray(HexMessage));
            hash = AESUtil.bytesToHex(bytes);
            //System.out.println(hash);
        } catch (Exception e) {
            System.out.println("Error HmacSHA256 ===========" + e.getMessage());
        }
        return hash;
    }

}
