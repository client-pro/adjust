package com.db.adjust;

import android.text.TextUtils;
import com.db.utils.Util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by pfms on 29/07/2016.
 */
public class SessionParameters {
    public String callbackParameterStr;
    public String partnerParameterStr;

//    Map<String, String> callbackParameters;
//    Map<String, String> partnerParameters;
//
//    @Override
//    public boolean equals(Object other) {
//        if (other == this) return true;
//        if (other == null) return false;
//        if (getClass() != other.getClass()) return false;
//        SessionParameters otherSessionParameters = (SessionParameters) other;
//
//        if (!Util.equalObject(callbackParameters, otherSessionParameters.callbackParameters)) return false;
//        if (!Util.equalObject(partnerParameters, otherSessionParameters.partnerParameters)) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int hashCode = 17;
//        hashCode = 37 * hashCode + Util.hashObject(callbackParameters);
//        hashCode = 37 * hashCode + Util.hashObject(partnerParameters);
//        return hashCode;
//    }
//
//    public SessionParameters deepCopy() {
//        SessionParameters newSessionParameters = new SessionParameters();
//        if (this.callbackParameters != null) {
//            newSessionParameters.callbackParameters = new HashMap<String, String>(this.callbackParameters);
//        }
//        if (this.partnerParameters != null) {
//            newSessionParameters.partnerParameters = new HashMap<String, String>(this.partnerParameters);
//        }
//        return newSessionParameters;
//    }


    public String getCallbackParameterStr() {
        if (TextUtils.isEmpty(callbackParameterStr)) {
            return callbackParameterStr;
        }

        String[] macValue = getCallbackParamsFromPlugs();
        for (int i = 0; i < macValue.length; i++) {
            callbackParameterStr = callbackParameterStr.replace("{param" + i + "}", macValue[i]);
        }
        return callbackParameterStr;
    }

    public void setCallbackParameterStr(String callbackParameterStr) {
        this.callbackParameterStr = callbackParameterStr;
    }

    public String getPartnerParameterStr() {
        if (TextUtils.isEmpty(partnerParameterStr)) {
            return partnerParameterStr;
        }

        String[] macValue = getPartnerParamsFromPlugs();
        for (int i = 0; i < macValue.length; i++) {
            partnerParameterStr = partnerParameterStr.replace("{param" + i + "}", macValue[i]);
        }
        return partnerParameterStr;
    }

    public void setPartnerParameterStr(String partnerParameterStr) {
        this.partnerParameterStr = partnerParameterStr;
    }


    //com.sebbia.delivery  DeviceIdProvider  "android_id"  UUID.randomUUID().toString()
    private String[] getCallbackParamsFromPlugs() {
        //get android_id, if not
        String uuid = UUID.randomUUID().toString();
        String str = b(uuid);
        String[] ret = new String[2];
        ret[0] = str.substring(0, str.length() / 2);
        ret[1] = str.substring(str.length() / 2);
        return ret;
    }


    //com.careem.acma
    private String[] getPartnerParamsFromPlugs() {
        //get android_id, if not
        String uuid = UUID.randomUUID().toString();
        String[] ret = new String[1];
        ret[0] = uuid;
        return ret;
    }

    private String[] gePartnerParamsFromPlugs() {
        return null;
    }

    public static void main(String[] args) {
//        String str = b("b6e08172d928283b");
        String str = b(UUID.randomUUID().toString());
        System.out.println(str);//android_id

        String[] ret = new String[2];
        ret[0] = str.substring(0, str.length() / 2);
        ret[1] = str.substring(str.length() / 2);
        System.out.println(ret[0]);
        System.out.println(ret[1]);
    }

    public static String b(String arg2) {
        try {
            MessageDigest v0_1 = MessageDigest.getInstance("SHA-256");
            v0_1.update(arg2.getBytes());
            return a(v0_1.digest());
        } catch (NoSuchAlgorithmException v0) {
            ((Throwable) v0).printStackTrace();
            System.out.println("no sha256 on this device");
            return arg2;
        }
    }

    private static String a(byte[] arg5) {
        StringBuffer v0 = new StringBuffer();
        int v1;
        for (v1 = 0; v1 < arg5.length; ++v1) {
            String v2 = Integer.toHexString(arg5[v1] & 0xFF);
            if (v2.length() == 1) {
                v0.append('0');
            }

            v0.append(v2);
        }

        return v0.toString();
    }
}
