package com.db.env;

import com.db.adjust.*;
import com.db.main.IOSDevice;
import org.json.JSONObject;

public class Machine {
    public DeviceInfo deviceInfo;

    public AdjustConfig adjustConfig;

    public ActivityState activityState;

    public SessionParameters sessionParameters;


    public static Machine parseMachine(JSONObject deviceJson, JSONObject appJson, String iso) {
        Machine machine = new Machine();
        machine.deviceInfo = DeviceInfo.parseDeviceInfo(deviceJson, appJson, iso);
        machine.adjustConfig = AdjustConfig.parseAdjustConfig(appJson);
        ActivityState activityState = new ActivityState();
        activityState.lastInterval = -1;
        activityState.pushToken = appJson.optString("pushToken", null);
        activityState.sessionCount = 1;
        activityState.sessionLength = -1;
        activityState.subsessionCount = -1;
        activityState.timeSpent = -1;
        machine.activityState = activityState;
        machine.sessionParameters = new SessionParameters();
        machine.sessionParameters.callbackParameterStr = appJson.optString("callback_params", null);
        System.out.println("machine.sessionParameters.callbackParameterStr:" + machine.sessionParameters.callbackParameterStr);
        return machine;
    }

    public static Machine parseMachineIOS(IOSDevice iosDevice, JSONObject appJson, String iso) {
        Machine machine = new Machine();
        machine.deviceInfo = DeviceInfo.parseDeviceInfoIOS(iosDevice, appJson, iso);
        machine.adjustConfig = AdjustConfig.parseAdjustConfig(appJson);
        ActivityState activityState = new ActivityState();
        activityState.lastInterval = -1;
        activityState.pushToken = appJson.optString("pushToken", null);
        activityState.sessionCount = 1;
        activityState.sessionLength = -1;
        activityState.subsessionCount = -1;
        activityState.timeSpent = -1;
        machine.activityState = activityState;
        machine.sessionParameters = new SessionParameters();
        machine.sessionParameters.callbackParameterStr = appJson.optString("callback_params", null);
        System.out.println("machine.sessionParameters.callbackParameterStr:" + machine.sessionParameters.callbackParameterStr);
        return machine;
    }

//    public static Machine parseMachine(String deviceStr,String appStr){
//        JSONObject deviceJson = new JSONObject(deviceStr);
//        JSONObject appJson = new JSONObject(appStr);
//        return parseMachine(deviceJson,appJson);
//    }

    public PackageBuilder createPackageBuilder(String iso) {
        PackageBuilder packageBuilder = new PackageBuilder(adjustConfig, deviceInfo, activityState, sessionParameters);
        packageBuilder.iso = iso;
        return packageBuilder;
    }

}






















