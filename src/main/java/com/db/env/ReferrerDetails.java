package com.db.env;

public class ReferrerDetails {
    public String installReferrer;
    public long referrerClickTimestampSeconds;
    public long installBeginTimestampSeconds;
    public boolean googlePlayInstantParam;
    public long referrerClickTimestampServerSeconds;
    public long installBeginTimestampServerSeconds;
    public String installVersion;

    public ReferrerDetails(String installReferrer,
                           long referrerClickTimestampSeconds,
                           long installBeginTimestampSeconds,
                           boolean googlePlayInstantParam,
                           long referrerClickTimestampServerSeconds,
                           long installBeginTimestampServerSeconds,
                           String installVersion
    ){
        this.installReferrer = installReferrer;
        this.referrerClickTimestampSeconds = referrerClickTimestampSeconds;
        this.installBeginTimestampSeconds = installBeginTimestampSeconds;
        this.googlePlayInstantParam = googlePlayInstantParam;
        this.referrerClickTimestampServerSeconds = referrerClickTimestampServerSeconds;
        this.installBeginTimestampServerSeconds = installBeginTimestampServerSeconds;
        this.installVersion = installVersion;
    }

    public String getInstallReferrer() {
        return installReferrer;
    }

    public long getReferrerClickTimestampSeconds() {
        return referrerClickTimestampSeconds;
    }

    public long getInstallBeginTimestampSeconds() {
        return installBeginTimestampSeconds;
    }

    public boolean getGooglePlayInstantParam() {
        return googlePlayInstantParam;
    }

    //todo may not have methods below from Reffer old version sdk
    public long getReferrerClickTimestampServerSeconds() {
        return referrerClickTimestampServerSeconds;
    }
    public long getInstallBeginTimestampServerSeconds() {
        return installBeginTimestampServerSeconds;
    }
    public String getInstallVersion() {
        return installVersion;
    }
}
