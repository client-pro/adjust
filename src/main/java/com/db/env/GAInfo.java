package com.db.env;

public class GAInfo {
    private String id;
    private boolean limitAdTrackingEnabled;
    private boolean gaidWithGps;//get gaid from gp service jar
//    private boolean gadSdk;

    public GAInfo(String id,boolean limitAdTrackingEnabled,boolean gaidWithGps){
        this.id = id;
        this.limitAdTrackingEnabled = limitAdTrackingEnabled;
        this.gaidWithGps = gaidWithGps;
    }

    public String getId(){
        return id;
    }

    public boolean isLimitAdTrackingEnabled(){
        return limitAdTrackingEnabled;
    }

    public boolean isGaidWithGps(){
        return gaidWithGps;
    }
}
