package android.net;

public class NetworkInfo {
    //0 TYPE_MOBILE;1 TYPE_WIFI; 6 TYPE_WIMAX; 9 TYPE_ETHERNET; 7 TYPE_BLUETOOTH;
    public int type;
    public String name;
    public boolean connectedOrConnecting;

    public int getType(){
        return type;
    }

    public String getName(){
        return name;
    }
    public boolean isConnectedOrConnecting(){
        return connectedOrConnecting;
    }
}
